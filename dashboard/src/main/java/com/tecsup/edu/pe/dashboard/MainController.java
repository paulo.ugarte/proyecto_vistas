package com.tecsup.edu.pe.dashboard;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
	
	@RequestMapping("/")
	public String index()
	{
		return "index";
	}
	
	@RequestMapping("/products")
	public String products()
	{
		return "products";
	}
	
	@RequestMapping("/salescreate")
	public String salescreate()
	{
		return "salescreate";
	}
	
	@RequestMapping("/saleslist")
	public String saleslist()
	{
		return "saleslist";
	}
	
	@RequestMapping("/home")
	public String home()
	{
		return "home";
	}
	@RequestMapping("/login")
	public String login()
	{
		return "login";
	}
	@RequestMapping("/register")
	public String register()
	{
		return "register";
	}
}

